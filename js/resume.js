(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#sideNav'
  });

})(jQuery); // End of use strict






function sticky_relocate_left() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('.left').addClass('mobile-sticky');
    } else {
        $('.left').removeClass('mobile-sticky');
    }
}

function sticky_relocate_right() {
    var window_top2 = $(window).scrollTop();
    var div_top2 = $('#sticky-anchor').offset().top;
    if (window_top2 > div_top2) {
        $('.right').addClass('mobile-sticky');
    } else {
        $('.right').removeClass('mobile-sticky');
    }
}

$(function () {
  //$(window).addEventListener("touchmove", sticky_relocate_right, false);
  setTimeout($(window).scroll(sticky_relocate_left),0);
  $(window).scroll(sticky_relocate_right);
  sticky_relocate_left();
  sticky_relocate_right();
});
//end sticky button




$('[lang="en"]').hide();

$('#switch-lang').click(function() {
  $('[lang="en"]').toggle();
  $('[lang="fr"]').toggle();
});
$('#switch-lang-mobile').click(function() {
  $('[lang="en"]').toggle();
  $('[lang="fr"]').toggle();
});




